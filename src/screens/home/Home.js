import React from 'react';
import { Button, NavLink } from 'reactstrap';
//components
import NavBar from '../../components/NavBar';



export default function Home(){
  return (
    <>
      <NavBar/>
      <div className='container'>
        <h1 className='display-4 mt-5'>Hi! I'm Redd</h1>
        <NavLink
          href="/posts"
        >
          <Button
            color='primary'
          >See Posts</Button>
        </NavLink>
        
      </div>
    </>
  )
}
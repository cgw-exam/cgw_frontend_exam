import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';


//loading
const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

//Screens
const Home = React.lazy(()=>import('./screens/home/Home'));
const Posts = React.lazy(()=>import('./screens/posts/Posts'));

function App() {
  return (
    <BrowserRouter>
      <React.Suspense fallback={loading}>
        <Switch>
          <Route exact path='/posts' name='Posts' component={Posts}/>
          <Route path='/' name='Home' component={Home}/>
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  );
}

export default App;

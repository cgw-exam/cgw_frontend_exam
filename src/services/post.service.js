import axios from 'axios';
import { apiURL } from './config'

const postURL = `${apiURL}posts`;

export default class Post {


  static async getPost() {
    return await axios.get(`${postURL}?_limit=10`);
  }

  constructor({title, body, userId}){
    this.title = title;
    this.body = body;
    this.userId = userId;
    return this;
  }

  getPostData(){
    return {
      title: this.title,
      body: this.body,
      userId: this.userId
    } 
  }

  async addPost(){
    return await axios.post(postURL, this.getPostData());
  }
}


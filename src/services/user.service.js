import axios from 'axios';
import { apiURL } from './config'

const userURL = `${apiURL}users`;
export default class User {
  static async getById(id){
    return axios.get(`${userURL}/${id}`);
  }
}
import React from 'react';
import { ListGroupItem } from 'reactstrap';

export default function PostRow({post, user}){
  return(
    <ListGroupItem className='d-flex p-3'>
      <img
        src='https://picsum.photos/200'
        height='100px'
        width='100px'
        className='rounded-circle mr-2'
        alt='user-profile'
      />
      <div>
        <h5>{post.title}</h5>
        <p><small>Posted by: {user.name}</small></p>
        <p className='text-secondary'>{post.body}</p>
      </div>
    </ListGroupItem>
  )
}
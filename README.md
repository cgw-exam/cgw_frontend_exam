## Installation

Upon cloning the file, install the needed dependencies.

Create a .env file and add the following:
REACT_APP_API_URL='https://jsonplaceholder.typicode.com/'

Serve the project via npm start.

Happy Hacking!


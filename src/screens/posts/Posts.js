import React, { useState, useEffect } from 'react';
import { Button, ListGroup } from 'reactstrap';
import NavBar from '../../components/NavBar';
import Post from '../../services/post.service';
import User from '../../services/user.service';
import PostForm from './PostForm';
import PostRow from './PostRow';

export default function Posts(){

  const [posts, setPosts] = useState([]);
  const [user, setUser] = useState({});
  const [open, setOpen] = useState(false);

  const userId = 1;

  useEffect(()=>{
    Post.getPost()
      .then(data=>{
        setPosts(data.data);
        User.getById(userId)
        .then(data=>{
          setUser(data.data);
        })
      })
  }, []);

  const submitForm = async ({title, body}) => {
    const newPost = new Post({title, body, userId});
    const data = await newPost.addPost();
    const newPosts = [data.data, ...posts];
    setPosts(newPosts);
    setOpen(false);
    console.log(data);
  }

  return(
    <>
      <NavBar/>
      <div className='container d-flex justify-content-center min-vh-100 '>
        <div className='col-lg-6 col-sm-12 bg-white'>
          <h1 className='my-3 text-center'>Posts</h1>
          <div className='text-center mb-3'>
            <Button color='success'
              onClick={()=>setOpen(true)}
            >+ Add Post</Button>
          </div>
          <ListGroup>
            {posts.map(post=>(
              <PostRow
                key={post.id}
                post={post}
                user={user}
              />
            ))}
          </ListGroup>
        </div>
      </div>
      <PostForm
        open={open}
        setOpen={setOpen}
        submitForm={submitForm}
      />
    </>
  )
}
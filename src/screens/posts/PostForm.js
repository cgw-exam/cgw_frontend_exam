import { ErrorMessage, Field, Form, Formik } from 'formik';
import React from 'react';
import { Button, FormGroup, Label, Modal, ModalBody, ModalHeader } from 'reactstrap';

import * as Yup from 'yup';

const postSchema = Yup.object().shape({
  title: Yup.string().required('Title is required'),
  body: Yup.string().required('Body is required'),

})

export default function PostForm({open, setOpen, submitForm}){

  const initialValues = {
    title: '',
    body: ''
  }

  return(
    <Modal
      isOpen={open}
    >
      <ModalHeader
        toggle={()=>setOpen(false)}
        className='bg-primary text-white'
      >Add Post</ModalHeader>
      <ModalBody>
        <Formik
          enableReinitialize
          initialValues={initialValues}
          validationSchema={postSchema}
          onSubmit={values=>submitForm(values)}
        >
          {({errors, touched})=>(
            <Form>
              <FormGroup>
                <Label htmlFor='title'>Title</Label>
                <Field
                  name="title"
                  type="text"
                  placeholder='Post Title'
                  className={'form-control' + (errors.title && touched.title ? ' is-invalid' : '')}
                />
                <ErrorMessage name="title" component="div" className="invalid-feedback" />
              </FormGroup>
              <FormGroup>
                <Label htmlFor='body'>Body</Label>
                <Field
                  name="body"
                  component="textarea"
                  placeholder='Post Body'
                  className={'form-control' + (errors.body && touched.body ? ' is-invalid' : '')}
                />
                <ErrorMessage name="body" component="div" className="invalid-feedback" />
              </FormGroup>
              <Button
                color='success'
              >Save Post</Button>
            </Form>
          )}
        </Formik>
      </ModalBody>
    </Modal>
  )
}